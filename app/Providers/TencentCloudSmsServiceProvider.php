<?php

namespace App\Providers;
use Illuminate\Support\ServiceProvider;
use TencentCloud\Common\Credential;
// 导入对应产品模块的client
use TencentCloud\Sms\V20190711\SmsClient;
// 导入可选配置类
use TencentCloud\Common\Profile\ClientProfile;
use TencentCloud\Common\Profile\HttpProfile;

class TencentCloudSmsServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {

        $this->app->singleton(SmsClient::class, function ($app) {
            $cred = new Credential(config('tencentcloud.default.secretId'), config('tencentcloud.default.secretKey'));

            $httpProfile = new HttpProfile('https://', config('tencentcloud.sms.endpoint'), config('tencentcloud.default.method'), config('tencentcloud.default.timeout'));

            if (config('tencentcloud.default.proxy')) {
                $httpProfile->setProxy(config('tencentcloud.default.proxy'));
            }

            $clientProfile = new ClientProfile(config('tencentcloud.default.signMethod'), $httpProfile);

            return new SmsClient($cred, config('tencentcloud.sms.region'), $clientProfile);
        });

        $this->app->alias(SmsClient::class, 'tencentcloudSms');
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
