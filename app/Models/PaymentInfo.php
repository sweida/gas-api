<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PaymentInfo extends Model
{
    // 接受的字段
    protected $fillable = [
        'user_id',
        'amount',
        'oauth_id',
        'createTime',
        'hicardOrderNo',
        'merchOrderNo',
        'payInfo',
        'respCode',
        'respMsg',
    ];

    // 表格隐藏的字段
    protected $hidden = [

    ];
}
