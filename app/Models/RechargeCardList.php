<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RechargeCardList extends Model
{
    // 接受的字段
    protected $fillable = [
        'user_id',
        'card_type',
        'common_number',
        'last_number',
    ];

    // 表格隐藏的字段
    protected $hidden = [

    ];
}
