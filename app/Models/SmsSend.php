<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use TencentCloud\Sms\V20190711\Models\SendSmsRequest;

class SmsSend extends Model
{
    /**
     * 发送短信 SendSms
     *
     * @param String $sdkappid 短信应用ID: 短信SdkAppid在 [短信控制台] 添加应用后生成的实际SdkAppid，示例如1400006666
     * @param String $sign 短信签名内容: 使用 UTF-8 编码，必须填写已审核通过的签名，签名信息可登录 [短信控制台] 查看
     * @param Array $phoneNumbers 下发手机号码，采用 e.164 标准，+[国家或地区码][手机号]: 示例如：+8613711112222， 其中前面有一个+号 ，86为国家码，13711112222为手机号，最多不要超过200个手机号
     * @param String $templdateId 模板 ID: 必须填写已审核通过的模板 ID。模板ID可登录 [短信控制台] 查看
     * @param Array $templateParams 模板参数: 若无模板参数，则设置为空
     * @param String $sessionContext 用户的 session 内容: 可以携带用户侧 ID 等上下文信息，server 会原样返回
     * @param String $extend 短信码号扩展号: 默认未开通，如需开通请联系 [sms helper]
     * @param String $senderId 国际/港澳台短信 senderid: 国内短信填空，默认未开通，如需开通请联系 [sms helper]
     * @return void
     */

    public function sms(String $sdkappid = '', String $sign = '', Array $phoneNumbers = [], String $templdateId = '', Array $templateParams = []) {
        // 实例化一个 sms 发送短信请求对象,每个接口都会对应一个request对象。
        $req = new SendSmsRequest();

        $req->SmsSdkAppid = $sdkappid;

        $req->Sign = $sign;

        $req->PhoneNumberSet = $phoneNumbers;

        $req->TemplateID = $templdateId;

        $req->TemplateParamSet = $templateParams;

        return $req;
    }
}


