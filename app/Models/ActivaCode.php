<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ActivaCode extends Model
{
    // 接受的字段
    protected $fillable = [
        'code', 'name', 'desc', 'one_time'
    ];

    // 表格隐藏的字段
    protected $hidden = [
        'updated_at'
    ];
}
