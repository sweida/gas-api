<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Station extends Model
{
    // 软删除
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    // 接受的字段
    protected $fillable = [
        'title',
        'province',
        'area',
        'city',
        'code_id',
        'address',
        'tel',
        'longitude',
        'latitude',
        'business_state',
        'business_start_time',
        'business_end_time',
        'sell_type',
        'desc',
        'original_price',
        'now_price',
        'nation_price',
        'shop_image',
    ];

    // 表格隐藏的字段
    protected $hidden = [
        'updated_at',
        'deleted_at'
    ];
}
