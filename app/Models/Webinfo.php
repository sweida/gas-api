<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Webinfo extends Model
{
    // 接受的字段
    protected $fillable = [
        'company',
        'address',
        'tel1',
        'tel2',
        'icp',
        'price_0_nation',
        'price_0_original',
        'price_0_now',
        'price_92',
        'price_95',
        'price_98',
        'entity_common_num',
        'virtual_common_num',
        'onekeygas_tip',
        'recharge_tip',
        'shop_qrcode',
    ];
}
