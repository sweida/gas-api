<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ActivateList extends Model
{
    // 接受的字段
    protected $fillable = [
        'code', 'user_id'
    ];
}
