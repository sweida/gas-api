<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    // 接受的字段
    protected $fillable = [
        'user_id',
        'order_id',
        'order_type',
        'remark',
        'amount',
        'station_id',
        'gas_original_price',
        'gas_now_price',
        'order_amount',
        'phone',
        'gas_type',
        'order_status', 
        'sign',
        'payment_time',
        'check_time',
        'card_type',
        'card_num',
        'reason',
    ];

    // 表格隐藏的字段
    protected $hidden = [

    ];

}
