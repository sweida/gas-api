<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Oauth extends Model
{
    // 接受的字段
    protected $fillable = [
        'user_id', 'oauth_id', 'oauth_type', 'info'
    ];

    // 数据填充时自动忽略这个字段
    public $timestamps = false;
}
