<?php

namespace App\Http\Requests;


class OrderRequest extends FormRequest
{
    public function rules()
    {
        switch (FormRequest::getPathInfo()){
            case '/api/v2/order/failure':
            case '/api/v2/order/detail':
            case '/api/v2/order/cancel':
            case '/api/v2/rechargeorder/audit':
                return [
                    'order_id' => ['required', 'exists:orders,order_id'],
                ];
            case '/api/v2/order/create':
                return [
                    'order_amount' => ['required'],
                ];
            case '/api/v2/order/list':
                return [

                ];
        }

    }

    public function messages()
    {
        return [
            'order_id.required' => '订单id不能未空',
            'order_id.exists' => '订单id不存在',
            'order_amount.required' => '订单金额不能为空',
        ];
    }  
}
