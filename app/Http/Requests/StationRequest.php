<?php

namespace App\Http\Requests;

class StationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function rules()
    {
        switch (FormRequest::getPathInfo()){
            case '/api/v2/station/edit':
            case '/api/v2/station/delete':
            case '/api/v2/station/restored':
            case '/api/v2/station/detail':
            case '/api/v2/station/reallydelete':
                return [
                    'id' => ['required', 'exists:stations,id']
                ];
            case '/api/v2/station/create':
                return [
                    'city' => ['required'],
                    'code_id' => ['required'],
                    'longitude' => ['required'],
                    'latitude' => ['required'],
                    'title' => ['required'],
                ];
            case '/api/v2/station/current_city_list':
                return [
                    'city' => ['required']
                ];
        }
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'id.required' => 'id不能为空',
            'id.exists' => 'id不存在',
            'city.required' => '城市不能为空',
            'title.required' => '标题不能为空',
            'code_id.required' => '油站编号不能为空',
            'longitude.required' => '经度不能为空',
            'latitude.required' => '纬度不能为空',
        ];
    }
}
