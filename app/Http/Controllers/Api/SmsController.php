<?php
namespace App\Http\Controllers\Api;

use App\Models\SmsSend;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;
use App\Http\Requests\SmsRequest;
use TencentCloud\Common\Exception\TencentCloudSDKException;
use Illuminate\Support\Facades\Redis;

class SmsController extends Controller
{

    // 发送短信
    public function send(SmsRequest $request, SmsSend $SmsSend)
    {
        $phone = "+86" . $request->phone;
        if ($this->is_robot($phone, 60)) {
            return $this->failed('操作太频繁，稍后再试', 200);
        }

        try {
            $sms = app('tencentcloudSms');

            $code = str_pad(random_int(1, 9999), 4, 0, STR_PAD_LEFT);

            $req = $SmsSend->sms(config('tencentcloud.sms.sdkappid'), config('tencentcloud.sms.sign'), [$phone], config('tencentcloud.sms.templateId'), [$code, '5']);

            $resp = $sms->SendSms($req);

            list($SendStatusSet, $RequestId) = array_values(json_decode($resp->toJsonString(), true));
            Log::info($SendStatusSet);
            if ($SendStatusSet[0]['SerialNo'] && $SendStatusSet[0]['Code'] === 'Ok') {
                Cache::put($RequestId, $SendStatusSet);
                // 设置验证码，错误次数归0
                Redis::setex('login_captcha:'.$phone, 300, $code);
                Redis::del('login_checkCount:'.$phone);
                $this->update_robot_time($phone);
                return $this->message('短信发送成功');
            } else if ($SendStatusSet[0]['Code'] === 'LimitExceeded.PhoneNumberDailyLimit') {
                return $this->failed('当前手机号当日发送短信条数已超过', 200);
            } else {
                Log::info($SendStatusSet);
                $sms_error = [
                    'key' => $RequestId,
                    'code' => $SendStatusSet[0]['Code'],
                    'message' => $SendStatusSet[0]['Message']
                ];
                // return $this->failed($SendStatusSet, 404);
                return $this->failed('短信发送失败', 200);
            }

        } catch (TencentCloudSDKException $e) {
            return $this->failed('短信系统出问题，请稍后再试', 200);
        }
    }

    // 检查是否机器人
    public function is_robot($id, $time = 60){
        // 如果没有last_action_time说明接口没被调用过
        $lastActionTime = Redis::get('lastActionTime:'.$id);
        if (!$lastActionTime) {
            return false;
        }

        $elapsed = time() - $lastActionTime;
        return !($elapsed > $time);
    }

    // 上一次操作时间
    public function update_robot_time($id){
        Redis::set('lastActionTime:'.$id, time());
    }

    // 校验验证码
    public function check_captcha($phone, $captcha){
        $phone = "+86" . $phone;
        $RedisCap = Redis::get('login_captcha:'.$phone);
        $checkCount = Redis::get('login_checkCount:'.$phone);

        if (!$RedisCap) 
            return $this->failed('请先获取验证码!', 200);

        if ($checkCount > 4)
            return $this->failed('错误次数太多，请重新获取验证码!', 200);

        if ($captcha != $RedisCap){
            Redis::incr('login_checkCount:'.$phone);
            return $this->failed('验证码不正确!', 200);
        }

        // 校验通过，删除验证码
        Redis::del('login_captcha:'.$phone);

        /*
        * 调用login接口，生成token
        * 未注册：跳转设置密码，
        * 已经注册：跳转个人中心，
        */
        // if ()
        // return true;
        return $this->message('校验通过');
    }

}
