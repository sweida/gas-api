<?php

namespace App\Http\Controllers\Api;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Models\RechargeCardList;

class RechargeCardListController extends Controller
{
    public function list(Request $request) {
        $userAuth = Auth::guard('api')->user();
        // 实体卡
        $data;
        $data['entity_list'] = RechargeCardList::where([
            'user_id' => $userAuth->user_id, 
            'card_type' => '实体卡'
        ])->orderBy('updated_at', 'desc')->get();
        // 虚拟卡
        $data['virtual_list'] = RechargeCardList::where([
            'user_id' => $userAuth->user_id, 
            'card_type' => '虚拟卡'
        ])->get();
        return $this->success($data);
    }

}
