<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Models\Order;
use Carbon\Carbon;

class DashboardController extends Controller
{
    public function show() 
    {
        $oneWeek = [Carbon::today()->subdays(7), Carbon::today()];
        // 营业额数据
        $data = [
            // 一键加油
            'oneKeyOrder' => Order::whereDate('created_at', Carbon::today())
                ->where('order_type', '1')
                ->whereIn('order_status', [2, 3, 8])
                ->selectRaw('
                    SUM(order_amount) as orderAmount,
                    SUM(amount) as amount,
                    count(*) as count')
                ->first(),
            // 作废订单
            'onKeyFailure' => Order::whereDate('created_at', Carbon::today())
                ->where('order_type', '1')
                ->whereIn('order_status', [7])
                ->selectRaw('SUM(amount) as amount, count(*) as count')
                ->first(),
            // 充值订单
            'rechargeOrder' => Order::whereDate('created_at', Carbon::today())
                ->where('order_type', '2')
                ->whereIn('order_status', [2, 5])
                ->selectRaw('SUM(amount) as amount, count(*) as count')
                ->first(),
            // 审核拒绝
            'rechargeFailure' => Order::whereDate('created_at', Carbon::today())
                ->where('order_type', '2')
                ->whereIn('order_status', [6])
                ->selectRaw('SUM(amount) as amount, count(*) as count')
                ->first(),
            'allOrder' => Order::whereDate('created_at', Carbon::today())
                ->whereIn('order_status', [2, 3, 5, 8])
                ->selectRaw('
                    SUM(order_amount) as orderAmount,
                    SUM(amount) as amount,
                    count(*) as count')
                ->first(),
            'oneWeek' => Order::whereBetween('created_at', $oneWeek)
                ->whereIn('order_status', [2, 3, 5, 8])
                ->selectRaw('
                    date(created_at) as date, 
                    SUM(amount) as amount, 
                    count(*) as count')
                ->groupBy('date')->get(),
        ];
        return $this->success($data);
    }
}
