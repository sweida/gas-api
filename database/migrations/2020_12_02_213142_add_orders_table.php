<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders',function (Blueprint $table) {
            $table->string('card_num')->nullable()->comment('油卡充值卡号');
            $table->string('card_type')->nullable()->comment('充值卡类型：实体卡/虚拟卡');
            $table->text('reason')->nullable()->comment('拒绝理由');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
