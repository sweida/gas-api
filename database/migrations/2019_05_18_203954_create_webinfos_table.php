<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWebinfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('webinfos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('company')->nullable()->comment('公司名');
            $table->string('address')->nullable()->comment('地址');
            $table->string('tel1')->nullable()->comment('客服电话');
            $table->string('tel2')->nullable()->comment('客服电话');
            $table->string('icp')->nullable()->comment('icp');
            $table->string('price_0_nation')->nullable()->comment('0#油价国标价');
            $table->string('price_0_original')->nullable()->comment('0#油价原价');
            $table->string('price_0_now')->nullable()->comment('0#油价优惠价');
            $table->string('price_92')->nullable()->comment('92油价国标');
            $table->string('price_95')->nullable()->comment('95油价国标');
            $table->string('price_98')->nullable()->comment('98油价国标');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('webinfos');
    }
}
