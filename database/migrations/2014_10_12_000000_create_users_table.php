<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('email')->unique()->nullable();
            $table->string('phone')->unique()->nullable();
            $table->text('avatar_url')->nullable()->comment('头像');
            // $table->string('password')->nullable();
            // $table->integer('captcha')->nullable()->comment('验证码');
            $table->string('is_activated')->nullable()->comment('是否激活');
            $table->text('intro')->nullable()->comment('保留字段');
            $table->string('is_admin')->nullable()->comment('管理员');
            $table->timestamps();
            // unique 唯一
            // nullable 可以为空
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
