<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_infos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('user_id')->comment('用户id');
            $table->string('amount')->comment('订单金额，单位分');
            $table->string('oauth_id')->comment('openid');
            $table->string('createTime')->nullable()->comment('订单生成支付时间');
            $table->string('hicardOrderNo')->nullable()->comment('汇卡订单号');
            $table->string('merchOrderNo')->nullable()->comment('我们生成的订单号');
            $table->text('payInfo')->nullable()->comment('支付信息');
            $table->string('respCode')->comment('状态码');
            $table->string('respMsg')->comment('状态信息');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment_infos');
    }
}
