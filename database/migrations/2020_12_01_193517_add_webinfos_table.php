<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddWebinfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('webinfos',function (Blueprint $table) {
            $table->string('entity_common_num')->nullable()->comment('油卡充值实体卡公共前缀');
            $table->string('virtual_common_num')->nullable()->comment('油卡充值虚拟卡公共前缀');
            $table->text('onekeygas_tip')->nullable()->comment('一键加油注意事项');
            $table->text('recharge_tip')->nullable()->comment('油卡充值注意事项');
            $table->string('shop_qrcode')->nullable()->comment('公众号二维码');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
