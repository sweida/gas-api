<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRechargeCardListsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // 用户充值卡号记录表
        Schema::create('recharge_card_lists', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('user_id')->comment('用户id');
            $table->string('card_type')->nullable()->comment('卡类型: 实体卡/虚拟卡');
            $table->string('common_number')->nullable()->comment('卡号公共前缀');
            $table->string('last_number')->nullable()->comment('卡号后6位');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('recharge_card_lists');
    }
}
