<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActivaCodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('activa_codes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('code')->unique()->comment('激活码');
            $table->string('name')->nullable()->comment('推荐人');
            $table->string('desc')->nullable()->comment('备注');
            $table->string('one_time')->nullable()->comment('是否一次性');
            // $table->unsignedInteger('user_id')->nullable()->comment('用户ID');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('activa_codes');
    }
}
