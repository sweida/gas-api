<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title')->comment('加油站名称');
            $table->string('province')->nullable()->default('广东省')->comment('省份');
            $table->string('city')->comment('城市');
            $table->string('area')->nullable()->comment('区');
            $table->string('address')->comment('地址');
            $table->string('code_id')->unique()->comment('加油站编码');
            $table->string('tel')->nullable()->comment('电话');
            $table->string('longitude')->comment('经度');
            $table->string('latitude')->comment('纬度');
            $table->integer('business_state')->nullable()->default(1)->comment('营业状态');
            $table->string('business_start_time')->nullable()->default('0:00')->comment('营业开始时间');
            $table->string('business_end_time')->nullable()->default('23:59')->comment('营业截止时间');
            $table->string('sell_type')->nullable()->comment('所售油品');
            $table->text('desc')->nullable()->comment('备注');
            $table->string('original_price')->nullable()->comment('柴油原价');
            $table->string('nation_price')->nullable()->comment('国标价');
            $table->string('now_price')->nullable()->comment('柴油现价');
            $table->string('shop_image')->nullable()->comment('店面图片');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stations');
    }
}
