<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRechargeOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('recharge_orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('user_id')->comment('用户id');
            $table->string('order_id')->unique()->comment('订单编号');
            $table->string('order_type')->default('油卡充值')->comment('订单类型');
            $table->integer('order_amount')->comment('订单原金额，单位分');
            $table->integer('amount')->nullable()->comment('优惠后订单金额，单位分');
            $table->string('realpay')->nullable()->comment('实际付款金额，单位分');
            $table->timestamp('payment_time')->nullable()->comment('付款时间');
            $table->timestamp('check_time')->nullable()->comment('审核时间');
            $table->string('phone')->comment('手机号');
            $table->string('gas_original_price')->nullable()->comment('油原价');
            $table->string('gas_now_price')->nullable()->comment('油现价');
            $table->string('station_id')->nullable()->comment('油站id');
            $table->string('gas_type')->default('0#')->comment('油品');
            $table->string('order_status')->default('1')->comment('订单状态');
            $table->string('refund_status')->nullable()->comment('退款状态');
            $table->integer('discount')->nullable()->comment('优惠金额');
            $table->integer('discount_id')->nullable()->comment('优惠券id');
            $table->string('remark')->nullable()->comment('备注信息');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('recharge_orders');
    }
}
