<?php

return [
	'default' => [
		// 腾讯云账户秘钥
		'secretId' => env('TENCENTCLOUD_SECRET_ID'),
		'secretKey' => env('TENCENTCLOUD_SECRET_KEY'),

		// HTTP 请求的超时时间（秒）
		'timeout' => 60.0,

		// 请求方式（默认为 POST）
		'method' => env('TENCENT_CLOUD_METHOD') ?? 'POST',

		// 指定签名算法（默认为 HmacSHA256）
		'signMethod' => env('TENCENT_CLOUD_SIGN_METHOD') ?? 'TC3-HMAC-SHA256',

		// 代理
		'proxy' => env('TENCENT_CLOUD_PROXY') ?? ''
	],

	'sms' => [
		// 指定接入地域域名（默认就近接入）
		'endpoint' => env('TENCENT_CLOUD_SMS_ENDPOINT') ?? 'sms.tencentcloudapi.com',

		// 地域参数（默认就近接入）
		'region' => env('TENCENT_CLOUD_SMS_REGION') ?? 'ap-shanghai',

		// 应用ID
		'sdkappid' => env('TENCENT_CLOUD_SMS_SDKAPPID'),

		// 短信签名
		'sign' => env('TENCENT_CLOUD_SMS_SIGN'),

		// 国际/港澳台短信 senderid: 国内短信填空，默认未开通，如需开通请联系 [sms helper](QQ: 3012203387)
		'senderId' => env('TENCENT_CLOUD_SMS_SENDERID') ?? '',

		// 模板ID
		'templateId' => env('TENCENT_CLOUD_SMS_TEMPLATEID') ?? '743618',
		'login_templateId' => '743618'
	]
];

