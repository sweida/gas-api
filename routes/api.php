<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });


// 版本号
Route::get('/version', function() {
    return ["name" => "laravel-gas-api", "author" => "sweida", "version" => "v2"];
});


Route::namespace('Api')->prefix('v2')->group(function () {
    Route::post('/signup','UserController@signup')->name('users.signup');
    Route::post('/login','UserController@login')->name('users.login');
    Route::get('/github','UserController@redirectToProvider')->name('users.githublogin');
    Route::get('/github/login','UserController@githubLogin')->name('users.githublogin');

    // 管理员登录
    Route::middleware('adminLogin')->group(function () {
        Route::post('/admin/login', 'UserController@login')->name('users.adminlogin');
    });
    Route::middleware(['api.refresh', 'adminRole'])->group(function () {
        Route::post('/admin/set', 'UserController@setAdmin')->name('users.setAdmin');
        Route::post('/admin/cancel', 'UserController@cancelAdmin')->name('users.cancelAdmin');
        Route::post('/admin/list', 'UserController@AdminList')->name('users.AdminList');
        Route::post('/user/list','UserController@list')->name('users.list');
    });

    //当前用户信息
    Route::middleware('api.refresh')->group(function () {
        Route::post('/logout', 'UserController@logout')->name('users.logout');
        Route::get('/user/info','UserController@info')->name('users.info');
        Route::post('/user','UserController@show')->name('users.show');
        Route::post('/setpassword', 'UserController@setPassword');
        Route::post('/check_activated', 'UserController@CheckActivated');
    });
    Route::post('/user/search','UserController@search')->name('users.search');
    Route::post('/user/resetpassword','UserController@resetPassword')->name('users.resetpassword');
    Route::post('/user/send_email','CommonController@send_email')->name('users.send_email');
    Route::post('/user/check_captcha','CommonController@check_captcha')->name('users.check_captcha');

    // 图片上传又拍云
    Route::middleware(['api.refresh', 'adminRole'])->group(function () {
        Route::post('/image/upload', 'ImageController@upload')->name('image.upload');
        Route::post('/image/delete', 'ImageController@delete')->name('image.delete');
    });

    // 图片广告模块
    Route::post('/ad/list', 'AdController@list')->name('ad.list');
    Route::post('/ad', 'AdController@show')->name('ad.show');
    Route::middleware(['api.refresh', 'adminRole'])->group(function () {
        Route::post('/ad/add', 'AdController@add')->name('ad.add');
        Route::post('/ad/edit', 'AdController@edit')->name('ad.edit');
        Route::post('/ad/delete','AdController@delete')->name('ad.delete');
        Route::post('/webinfo/setting', 'WebinfoController@setting')->name('webinfo.setting');
    });

    // 网站信息模块
    Route::get('/webinfo/read', 'WebinfoController@read')->name('webinfo.read');

    // 发送短信
    Route::post('/sendsms', 'SmsController@send')->name('sms.send');
    Route::post('/check_captcha', 'UserController@check_captcha')->name('sms.check_captcha');

    // 加油站
    Route::post('/station/search','StationController@search')->name('station.search');
    Route::post('/station/detail','StationController@detail')->name('station.detail');
    Route::post('/station/list', 'StationController@list')->name('station.list');
    Route::get('/station/citylist', 'StationController@citylist')->name('station.citylist');
    Route::post('/station/current_city_list', 'StationController@currentCityList')->name('station.cityList');

    Route::middleware(['api.refresh', 'adminRole'])->group(function () {
        Route::post('/station/create', 'StationController@create')->name('station.create');
        Route::post('/station/edit', 'StationController@edit')->name('station.edit');
        Route::post('/station/delete', 'StationController@delete')->name('station.delete');
        Route::post('/station/restored', 'StationController@restored')->name('station.restored');
        Route::post('/station/reallydelete', 'StationController@reallyDelete')->name('station.reallyDelete');
    });

    // 绑定手机号
    Route::post('/bindphone', 'UserController@bindPhone');
    
    
    // 微信授权登录
    Route::any('/wechatauth', 'WechatController@wechat');

    Route::middleware(['wechat.oauth'])->group(function () {
        Route::get('/wechat/auth', 'WechatController@wechatAuth')->name('user.wechat_auth');
    });

    // 订单
    Route::middleware('api.refresh')->group(function () {
        // 一键加油 二维码
        Route::post('/order/create', 'OrderController@createOrder')->name('order.creareOrder');
        Route::post('/order/list', 'OrderController@orderList')->name('order.orderList');
        Route::post('/order/cancel', 'OrderController@cancelOrder')->name('order.cancelOrder');
        Route::post('/order/huika', 'OrderController@huikaOrder')->name('order.huikaOrder');
        Route::post('/order/detail', 'OrderController@detail')->name('order.detail');
        Route::post('/order/payment_status', 'OrderController@paymentStatus')->name('order.paymentStatus');

        // 油卡充值
        Route::post('/rechargeorder/create', 'OrderController@createRechargeOrder')->name('order.createRechargeOrder');
        Route::post('/rechargeorder/audit', 'OrderController@audit')->name('order.audit');
        Route::get('/recharge/cardlist', 'RechargeCardListController@list')->name('order.list');
        
    });
    
    // 后台查询
    Route::middleware(['api.refresh', 'adminRole'])->group(function () {
        Route::post('/order/search', 'OrderController@search')->name('order.search');
        Route::post('/order/all_list', 'OrderController@allOrderList')->name('order.allOrderList');
        Route::post('/order/failure', 'OrderController@failureOrder')->name('order.failureOrder');
    });
    
    // 支付信息列表
    Route::middleware('api.refresh')->group(function () {
        Route::post('/payment/list', 'PaymentInfoController@list')->name('paymentInfo.list');
        Route::post('/payment/search', 'PaymentInfoController@search')->name('paymentInfo.search');
        Route::post('/payment/query', 'PaymentInfoController@query')->name('paymentInfo.query');
        Route::post('/payment/query_result', 'PaymentInfoController@queryResult')->name('paymentInfo.queryResult');
        Route::post('/activacode/check', 'ActivaCodeController@check')->name('ActivaCode.check');
        Route::post('/payment/text', 'PaymentInfoController@text')->name('paymentInfo.text');
    });
    
    Route::any('/payment/callback', 'PaymentInfoController@callback')->name('paymentInfo.callback');

    // 生成随机激活码
    Route::middleware(['api.refresh', 'adminRole'])->group(function () {
        Route::post('/activacode/create', 'ActivaCodeController@create')->name('ActivaCode.create');
        Route::post('/activacode/list', 'ActivaCodeController@list')->name('ActivaCode.list');
    });
   
    // 壳牌
    Route::get('/kepai/purchase', 'KepaiController@purchase')->name('ActivaCode.purchase');
    Route::get('/kepai/query_order', 'KepaiController@queryOrder')->name('ActivaCode.queryOrder');
    Route::get('/kepai/accountbalance', 'KepaiController@accountbalance')->name('ActivaCode.accountbalance');
    Route::any('/kepai/order_callback', 'KepaiController@OrderCallback')->name('ActivaCode.OrderCallback');
    Route::any('/kepai/verification_callback', 'KepaiController@VerificationCallback')->name('ActivaCode.VerificationCallback');

    Route::get('/wechat/get_access_token', 'WechatController@getAccess_token')->name('wechat.getAccess_token');
    Route::get('/wechat/send_message', 'WechatController@send')->name('wechat.send');
    
    // 营业额
    Route::get('/dashboard', 'DashboardController@show')->name('Dashboard.show');
});

